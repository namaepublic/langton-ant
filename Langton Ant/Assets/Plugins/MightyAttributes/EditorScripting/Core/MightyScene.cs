﻿using System;
using System.Reflection;

#if UNITY_EDITOR

namespace MightyAttributes.Editor
{
    public class MightyScene
    {
        private MightyMembersCache<FieldInfo> m_mightySerializeFieldsCache;

        public void OnEnable(Type type, Object ob)
        {
        }

        private void CacheFields(Type type, MightyContext context)
        {
            m_mightySerializeFieldsCache = new MightyMembersCache<FieldInfo>();
         
            
        }

        public void OnSceneGUI()
        {
        }
    }
}
#endif