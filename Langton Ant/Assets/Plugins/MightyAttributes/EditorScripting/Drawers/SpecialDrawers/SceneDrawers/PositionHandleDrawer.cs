﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

namespace MightyAttributes.Editor
{
    public class PositionHandleDrawer : BaseSceneDrawer<PositionHandleAttribute>
    {
        protected override void OnGUI(MightySerializedField serializedField, PositionHandleAttribute attribute)
        {
            var property = serializedField.Property;

            switch (property.propertyType)
            {
                case SerializedPropertyType.Vector2:
                {
                    var position = property.vector2Value;

                    EditorGUI.BeginChangeCheck();
                    
                    if (attribute.IsRelative && serializedField.Context.Target is MonoBehaviour script)
                    {
                        var tfmPosition = (Vector2) script.transform.position;
                        position += tfmPosition;
                        if (attribute.DisplayName)
                            Handles.Label(position, property.displayName);
                        position = Handles.PositionHandle(position, Quaternion.identity);
                        position -= tfmPosition;
                    }
                    else
                    {
                        if (attribute.DisplayName)
                            Handles.Label(position, property.displayName);
                        position = Handles.PositionHandle(position, Quaternion.identity);
                    }

                    if (EditorGUI.EndChangeCheck())
                    {
                        property.vector2Value = position;
                        property.serializedObject.ManageValueChanged();
                    }
                    break;
                }
                case SerializedPropertyType.Vector3:
                {
                    var position = property.vector3Value;

                    EditorGUI.BeginChangeCheck();
                    
                    if (attribute.IsRelative && serializedField.Context.Target is MonoBehaviour script)
                    {
                        var tfmPosition = script.transform.position;
                        position += tfmPosition;
                        if (attribute.DisplayName)
                            Handles.Label(position, property.displayName);
                        position = Handles.PositionHandle(position, Quaternion.identity);
                        position -= tfmPosition;
                    }
                    else
                    {
                        if (attribute.DisplayName)
                            Handles.Label(position, property.displayName);
                        position = Handles.PositionHandle(position, Quaternion.identity);
                    }

                    if (EditorGUI.EndChangeCheck())
                    {
                        property.vector3Value = position;
                        property.serializedObject.ManageValueChanged();
                    }
                    break;
                }
                default:
                    MightyGUIUtilities.DrawHelpBox($"{nameof(PositionHandleAttribute)} can be used only on Vector2 or Vector3 fields");
                    return;
            }
        }

        protected override void Enable(BaseMightyMember mightyMember, PositionHandleAttribute attribute)
        {
        }

        protected override void ClearCache()
        {
        }
    }
}
#endif
