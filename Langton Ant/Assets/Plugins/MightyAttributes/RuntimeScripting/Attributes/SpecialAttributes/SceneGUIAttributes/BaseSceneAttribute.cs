﻿namespace MightyAttributes
{
    public abstract class BaseSceneAttribute : BaseSpecialAttribute
    {
        public bool DisplayName { get; }

        protected BaseSceneAttribute(bool displayName) => DisplayName = displayName;
    }
}