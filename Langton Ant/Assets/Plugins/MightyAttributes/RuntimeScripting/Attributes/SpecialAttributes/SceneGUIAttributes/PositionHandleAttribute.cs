﻿namespace MightyAttributes
{
    public class PositionHandleAttribute : BaseSceneAttribute
    {
        public bool IsRelative { get; }
        
        public PositionHandleAttribute(bool isRelative = false, bool displayName = true) : base(displayName) => IsRelative = isRelative;
    }
}