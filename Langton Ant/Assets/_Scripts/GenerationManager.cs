using MightyAttributes;
using TMPro;
using UnityEngine;

public class GenerationManager : MonoBehaviour
{
    private const string MAX = "MAX";

    [SerializeField, Required] private GameObject _canvas;

    [SerializeField, Required] private GameObject _lockImage;
    [SerializeField, Required] private GameObject _pauseImage;
    [SerializeField, Required] private GameObject _pauseKeysTexts;

    [Space] [SerializeField, Required] private TextMeshProUGUI _speedLabel;
    [SerializeField, Required] private TextMeshProUGUI _fpsLabel;
    public float fpsRefreshDuration;

    [Space] [Required] public AntRenderer antRenderer;
    [Required] public GridRenderer gridRenderer;

    public float inputSpeedDelay;
    public int speedStepsAmount;
    public AnimationCurve speedCurve;

    private bool m_isPlaying;

    private float m_fpsDisplayTimer;

    private float m_inputSpeedTimer;
    private int m_speedStep;
    private float m_updateDelay;
    private float m_updateTimer;

    private GridModel m_gridModel;

    private void Start()
    {
        m_fpsDisplayTimer = 0;
        m_speedStep = speedStepsAmount;
        m_updateDelay = 0;
        _speedLabel.text = MAX;

        m_gridModel = gridRenderer.gridModel;
        _lockImage.SetActive(m_gridModel.lockBorder);

        Reset();
    }

    private void Reset()
    {
        gridRenderer.CreateGridTexture(false);
        antRenderer.CreateAnt();
        Play(false);
    }

    public void Play(bool play)
    {
        m_isPlaying = play;
        _pauseImage.SetActive(!play);
        _pauseKeysTexts.SetActive(!play);
        
        m_updateTimer = 0;
    }

    public void RandomGrid()
    {
        gridRenderer.CreateGridTexture(true);
        antRenderer.CreateAnt();
        Play(false);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.H))
            _canvas.SetActive(!_canvas.activeSelf);

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
            return;
        }

        if (Input.GetKeyUp(KeyCode.Backspace))
        {
            Reset();
            return;
        }

        if (Input.GetKeyUp(KeyCode.R))
        {
            RandomGrid();
            return;
        }

        if (Input.GetKeyDown(KeyCode.L))
        {
            m_gridModel.lockBorder = !m_gridModel.lockBorder;
            _lockImage.SetActive(m_gridModel.lockBorder);
            Reset();
            return;
        }

        var deltaTime = Time.deltaTime;

        ChangeSpeed(deltaTime);
        UpdateGeneration(deltaTime);
        DisplayFPS(deltaTime);
    }

    private void ChangeSpeed(float deltaTime)
    {
        if (Input.GetKeyDown(KeyCode.Alpha0) || Input.GetKeyDown(KeyCode.Keypad0))
        {
            m_speedStep = speedStepsAmount;
            m_updateDelay = 0;
            _speedLabel.text = MAX;

            return;
        }

        if (m_inputSpeedTimer < inputSpeedDelay)
        {
            m_inputSpeedTimer += deltaTime;
            return;
        }

        m_inputSpeedTimer = 0;

        if (Input.GetKey(KeyCode.Plus) || Input.GetKey(KeyCode.KeypadPlus))
        {
            if (++m_speedStep >= speedStepsAmount)
            {
                m_speedStep = speedStepsAmount;
                m_updateDelay = 0;
                _speedLabel.text = MAX;
            }
            else
            {
                m_updateDelay = Time.smoothDeltaTime * 1 / speedCurve.Evaluate((float) m_speedStep / speedStepsAmount);
                _speedLabel.text = Mathf.CeilToInt(1 / m_updateDelay).ToString();
            }
        }

        if (Input.GetKey(KeyCode.Minus) || Input.GetKey(KeyCode.KeypadMinus))
        {
            if (--m_speedStep <= 0) m_speedStep = 0;

            m_updateDelay = Time.smoothDeltaTime * 1 / speedCurve.Evaluate((float) m_speedStep / speedStepsAmount);
            _speedLabel.text = Mathf.CeilToInt(1 / m_updateDelay).ToString();
        }
    }

    private void UpdateGeneration(float deltaTime)
    {
        if (IsPlaying() || Input.GetKey(KeyCode.Return))
        {
            if (m_updateTimer < m_updateDelay)
                m_updateTimer += deltaTime;
            else
            {
                m_updateTimer = 0;
                antRenderer.UpdateAnt();
            }

            return;
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
            antRenderer.UpdateAnt();
    }

    private bool IsPlaying()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            Play(!m_isPlaying);

        return m_isPlaying;
    }

    private void DisplayFPS(float deltaTime)
    {
        if (m_fpsDisplayTimer < fpsRefreshDuration)
        {
            m_fpsDisplayTimer += deltaTime;
            return;
        }

        m_fpsDisplayTimer = 0;

        _fpsLabel.text = Mathf.CeilToInt(1 / deltaTime).ToString();
    }
}