using MightyAttributes;
using UnityEngine;
using UnityEngine.UI;

public class ToggleImageColor : MonoBehaviour
{
    [GetComponentButton] public Image image;
    [GetComponentButton] public Toggle toggle;
    public Color onColor, offColor;

    private void Start() => Toggle(toggle.isOn);

    public void Toggle(bool on) => image.color = @on ? onColor : offColor;
}
