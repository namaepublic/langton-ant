using UnityEngine;

public class Ant
{
    public Vector2Int Position { get; private set; }

    public Ant(Vector2Int position) => Position = position;

    public void UpdateAntPosition(GridModel gridModel, Transform orientationTransform)
    {
        var full = gridModel.IsCellFull(Position);

        if (full)
            orientationTransform.Rotate(Vector3.forward * 90);
        else
            orientationTransform.Rotate(Vector3.forward * -90);

        gridModel.ToggleCell(Position);

        var up = orientationTransform.up;
        Position += new Vector2Int((int) up.x, (int) up.y);
    }
}