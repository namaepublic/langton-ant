using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class GridModel : ScriptableObject
{
    public delegate void CellUpdateEvent(Vector2Int position, bool full);

    public bool lockBorder;

    public Vector2Int size;

    private bool[,] m_cells;
    private Dictionary<Vector2Int, bool> m_cellFullStates;

    private CellUpdateEvent m_onCellUpdate;

    public IEnumerable<bool> Init(bool random, CellUpdateEvent onCellUpdate)
    {
        m_onCellUpdate = onCellUpdate;
        var (width, height) = (size.x, size.y);

        if (lockBorder)
        {
            m_cells = new bool[width, height];

            for (var x = 0; x < width; x++)
            for (var y = 0; y < height; y++)
                yield return m_cells[x, y] = random && Random.value > .5f;
        }

        else
        {
            m_cellFullStates = new Dictionary<Vector2Int, bool>();

            for (var x = 0; x < width; x++)
            for (var y = 0; y < height; y++)
                yield return random && Random.value > .5f;
        }
    }

    public bool IsCellFull(Vector2Int position)
    {
        if (!lockBorder)
            return m_cellFullStates.TryGetValue(position, out var full) && full;

        var (x, y) = (position.x, position.y);
        if (x < 0 || x >= size.x || y < 0 || y >= size.y)
            return false;

        return m_cells[x, y];
    }

    public void ToggleCell(Vector2Int position)
    {
        bool full;
        var (x, y) = (position.x, position.y);
        
        if (lockBorder)
        {
            if (x < 0 || x >= size.x || y < 0 || y >= size.y) return;

            full = m_cells[x, y] = !m_cells[x, y];
        }
        else
        {
            m_cellFullStates.TryGetValue(position, out full);
            full = m_cellFullStates[position] = !full;
            
            if (x < 0 || x >= size.x || y < 0 || y >= size.y) return;
        }

        m_onCellUpdate(position, full);
    }

    public void FillCell(Vector2Int position, bool full)
    {
        var (x, y) = (position.x, position.y);
        if (lockBorder)
        {
            if (x < 0 || x >= size.x || y < 0 || y >= size.y) return;

            m_cells[x, y] = full;
        }
        else
        {
            m_cellFullStates[position] = full;

            if (x < 0 || x >= size.x || y < 0 || y >= size.y) return;
        }

        m_onCellUpdate(position, full);
    }
}