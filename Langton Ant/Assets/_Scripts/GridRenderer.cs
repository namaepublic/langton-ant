using MightyAttributes;
#if UNITY_EDITOR
using MightyAttributes.Editor;
using UnityEditor;
#endif
using UnityEngine;

public class GridRenderer : MonoBehaviour
{
    [Required, GetComponentButton] public new SpriteRenderer renderer;

    [Required, CustomDrawer("GridModelDrawer")]
    public GridModel gridModel;

    public float cellSize;
    public Color fullColor;
    public Color emptyColor;

    private GridModel.CellUpdateEvent m_onCellUpdate;

    private Texture2D m_gridTexture;

    private void Awake() => m_onCellUpdate = RenderCell;

    public void CreateGridTexture(bool random)
    {
        var (width, height) = (gridModel.size.x, gridModel.size.y);

        var pixels = new Color[width * height];

        var i = 0;
        foreach (var full in gridModel.Init(random, m_onCellUpdate))
            pixels[i++] = (full ? fullColor : emptyColor);

        m_gridTexture = new Texture2D(width, height) {filterMode = FilterMode.Point};

        var sprite = Sprite.Create(m_gridTexture, new Rect(0, 0, width, height), new Vector2(.5f, .5f), 100);
        renderer.sprite = sprite;
        renderer.transform.localScale = new Vector3(cellSize, cellSize);

        m_gridTexture.SetPixels(pixels);
        m_gridTexture.Apply();
    }

    private void RenderCell(Vector2Int position, bool full)
    {
        m_gridTexture.SetPixel(position.x, position.y, full ? fullColor : emptyColor);
        m_gridTexture.Apply();
    }

#if UNITY_EDITOR
    [Button(executeInPlayMode: false), Order(10)]
    private void CreateRandomGrid()
    {
        if (EditorApplication.isPlaying) return;

        if (!renderer || !gridModel) return;

        Awake();
        CreateGridTexture(true);
    }

    private void GridModelDrawer(MightySerializedField serializedField)
    {
        EditorGUILayout.BeginHorizontal();

        var property = serializedField.Property;
        EditorGUILayout.ObjectField(property, EditorGUIUtility.TrTextContent(property.displayName));

        if (MightyGUIUtilities.DrawIconButton(IconName.PLUS, GUILayout.Width(30)))
        {
            var asset = ScriptableObject.CreateInstance(serializedField.PropertyType);
            var path = EditorUtility.SaveFilePanel("Create Grid Model", Application.dataPath, "GridModel", "asset");
            AssetDatabase.CreateAsset(asset, FileUtilities.GetAssetsPath(path));

            property.objectReferenceValue = asset;
        }

        EditorGUILayout.EndHorizontal();
    }
#endif
}