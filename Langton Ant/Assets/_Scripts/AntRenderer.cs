using MightyAttributes;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class AntRenderer : MonoBehaviour
{
    [Required, GetComponentButton] public new SpriteRenderer renderer;
    [Required] public GridRenderer gridRenderer;
    [Required] public Grid grid;

    public Vector2Int startPosition;
    public Vector2Int startOrientation;

    private Ant m_ant;

    private Transform m_rendererTransform;
    private GridModel m_gridModel;

    private void Awake()
    {
        m_gridModel = gridRenderer.gridModel;
        m_rendererTransform = renderer.transform;
    }

    public void CreateAnt()
    {
        m_rendererTransform.up = new Vector3(startOrientation.x, startOrientation.y);
        m_rendererTransform.localScale = Vector3.one * gridRenderer.cellSize / gridRenderer.renderer.sprite.pixelsPerUnit;

        m_ant = new Ant(startPosition);
        ApplyPosition();
    }

    public void UpdateAnt()
    {
        m_ant.UpdateAntPosition(gridRenderer.gridModel, m_rendererTransform);
        ApplyPosition();
    }

    private void ApplyPosition()
    {
        var gridSize = m_gridModel.size;
        var halfWidth = gridSize.x / 2;
        var halfHeight = gridSize.y / 2;

        var worldPosition = grid.GetCellCenterWorld(new Vector3Int(m_ant.Position.x - halfWidth, m_ant.Position.y - halfHeight, 0));
        worldPosition.z *= -1;
        m_rendererTransform.position = worldPosition;
    }

#if UNITY_EDITOR
    [OnInspectorGUI]
    private void OnInspectorGUI()
    {
        if (EditorApplication.isPlaying) return;

        if (!renderer || !gridRenderer || !grid) return;

        Awake();
        CreateAnt();
    }
#endif
}